#ifndef STAT_H
#define STAT_H


int Seed;
double ArrayA[10], ArrayB[10];
double SumA, SumB;
double Coef;

void InitSeed ();

void Calc_Sum_Mean(double Array[], double *Sum, double *Mean);

double Square(double x);

void Calc_Var_Stddev(double Array[], double Mean, double *Var, double *Stddev);

void Calc_LinCorrCoef(double ArrayA[],double ArrayB[],double MeanA,double MeanB /*, *Coef*/);

void Initialize(double Array[]);

int RandomInteger();


#endif /* STAT_H */
